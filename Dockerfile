FROM postgres:15-alpine

# in your scenario you may want to curl it form the pg-deploy repo
# curl URL/pgdeploy--x.x.x.sql /usr/local/share/postgresql/extension
# curl URL/pgdeploy.control /usr/local/share/postgresql/extension
COPY pgdeploy--0.0.2.sql pgdeploy.control /usr/local/share/postgresql/extension/

ENTRYPOINT ["docker-entrypoint.sh"]
EXPOSE 5432
CMD ["postgres"]
