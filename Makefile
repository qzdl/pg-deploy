
EXTENSION = pgdeploy
EXTVERSION = $(shell cat ./VERSION)
EXTENSION_FILE = $(EXTENSION)--$(EXTVERSION).sql

# pgxs variables
DATA = $(EXTENSION)--$(EXTVERSION).sql
EXTRA_CLEAN = $(EXTENSION_FILE)

PG_CONFIG ?= pg_config
PGUSER	?= postgres
PGXS := $(shell $(PG_CONFIG) --pgxs)
include $(PGXS)

TESTS = $(sort $(wildcard test/sql/*.sql))
REGRESS = $(patsubst test/sql/%.sql,%,$(TESTS))
REGRESS_OPTS = --inputdir=test --load-language=plpgsql

# if local docker repository is used need to start daemon
DOCKER_HOST ?= 5000
DOCKER_SERVER ?= 5000
DOCKER_REGISTRY_RUNNING = $(shell docker container ps | grep -c registry:2)


EXTENSION_FILELIST = src/create_extension.sql src/reconcile_schema.sql $(sort $(filter-out $(wildcard src/reconcile_schema.sql), $(wildcard src/*.sql)))
build: $(EXTENSION_FILELIST)
	cat $^ > $(EXTENSION_FILE)


docker-build: build
	docker build . -t $(EXTENSION):$(EXTVERSION) && /


docker-publish: docker-build

ifeq ($(HOST),localhost)
	$(DOCKER_REGISTRY_RUNNING) || sudo docker run -d -p $(DOCKER_HOST):$(DOCKER_SERVER) --restart=always --name local_registry 'registry:2'
else
	docker login
endif
	sudo docker tag $(EXTENSION):$(EXTVERSION) $(HOST)/$(EXTENSION):$(EXTVERSION) && /
	sudo docker push $(HOST)/$(EXTENSION):$(EXTVERSION)
