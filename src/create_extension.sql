/*
   create_extension.sql: the first step of creating an extension
*/

-- complain if sourced in psql, rather than via CREATE EXTENSION
\echo Use "CREATE EXTENSION pgdeploy" to load this file. \quit


/*
   Since 14.5 we need to omit the CREATE SCHEMA part of the script otherwise the
      CREATE EXTENSION step fails.
      See [Changelog 1st entry](https://www.postgresql.org/docs/14/release-14-5.html#id-1.11.6.5.4)
*/

DO $$
DECLARE version FLOAT4;
BEGIN

    SELECT current_setting('server_version')::FLOAT4 into version;
    IF version < 14.5 THEN
            CREATE EXTENSION pgdeploy;
    END IF;
END $$;
